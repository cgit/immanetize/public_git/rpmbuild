#!/bin/bash
rm -rf ~/rpmbuild/MOCK
SPEC="$(find ~/rpmbuild/SPECS/ -name \*.spec)"
mock --buildsrpm -r fedora-rawhide-x86_64 \
  --spec $SPEC --sources SOURCES/ \
  --resultdir=/home/pete/rpmbuild/MOCK || ( 
    read -p "Build failed, press enter for log"
    less ~/rpmbuild/MOCK/build.log
    exit 1 
    )

SRPM="$(find ~/rpmbuild/MOCK -name \*.src.rpm)"
mock -r fedora-rawhide-x86_64 --resultdir=/home/pete/rpmbuild/MOCK \
  --rebuild $SRPM || ( 
    read -p "rebuild failed, press enter for log"
    less ~/rpmbuild/MOCK/build.log 
    exit 1
    )

mv /home/pete/rpmbuild/MOCK/*.src.rpm /home/pete/rpmbuild/SRPMS/
